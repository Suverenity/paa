#include "helpers.h"
#include "initializer.h"

#include <cmath>
#include <cstring>
#include <ostream>
#include <chrono>

void saveMesh(paa::Vector2d& mesh, std::string prefix,  std::string path, unsigned int iteration)
{
    using namespace std;
    ofstream f;
    std::ostringstream ostr;
    ostr << prefix << setfill('0') << setw(5) << iteration;

    f.open(path + "/" + ostr.str());
    for( auto y = 0u; y < mesh.m_ySize; ++y )
    {
        for( auto x = 0u; x < mesh.m_xSize; ++x )
        {
            f << x << " " << y << " " << mesh[y][x] << endl;
        }
        f << endl;
    }
    f.close();
}

void saveParallelMesh(paa::MpiWrapper& mpi, paa::Vector2d& partMesh, 
        paa::Initalizer& init, std::string prefix,
        std::string path, unsigned int iteration)
{
    std::vector<int> recieve;
    if( init.procRank == 0 )
    {
        recieve.resize(init.mpiSize, 0);
    }
    mpi.gatherInts(partMesh.internalVector().size(), recieve);

    std::vector<float> globalArray(0,0);
    if( init.procRank == 0 )
    {
        globalArray.resize(init.nxGlobal * init.nyGlobal);
    }
    mpi.gatherArrays(partMesh.internalVector(), globalArray, recieve);

    if( init.procRank == 0 )
    {
        std::vector<paa::Initalizer> procsInfo;
        for(auto i= 0; i < init.mpiSize; ++i)
        {
            paa::Initalizer tmpInit;
            tmpInit.initParallelGridStats(mpi, i, init.mpiSize, init.x1Global, init.x2Global,
                    init.y1Global, init.y2Global, init.nxGlobal, init.nyGlobal, init.xGrid, 
                    init.yGrid);
            procsInfo.push_back(tmpInit);
        }
        paa::Vector2d sortedMesh(init.nxGlobal, init.nyGlobal);
        int displacement = 0;

        for(auto& info: procsInfo)
        {
            auto xCoord = procsInfo[0].nxLocal*info.procXCoord;
            auto yCoord = procsInfo[0].nyLocal*info.procYCoord;
            for(auto numRows = 0; numRows < info.nyLocal; ++numRows)
            {
                std::memcpy(&sortedMesh[yCoord + numRows][xCoord],
                        &globalArray[displacement + numRows* info.nxLocal], 
                        info.nxLocal * sizeof(float));
            }
            displacement += info.nxLocal*info.nyLocal;
        }
        saveMesh(sortedMesh, prefix, path, iteration);
    }
    mpi.barrier();
}

paa::Vector2d computeConvolution(paa::Vector2d& u0, float time, float x1, float dx, float y1, float dy)
{
    std::cout << "convolution with time: " << time << "x:(" << x1 <<","<<dx << "),y:(" << y1 << ","<<dy << ")" << std::endl;
    using namespace paa;
    saveMesh(u0, "u0", ".", 0);
    Vector2d result(u0.m_xSize, u0.m_ySize, 0.);
    Vector2d gaussian(u0.m_xSize, u0.m_ySize, 0.);
    auto start = std::chrono::high_resolution_clock::now();
    auto koef = 1/(4*M_PI*time);
    std::cout << koef << std::endl;
    auto sigma = 1/(4*time);
    for(auto j=0u; j < gaussian.m_ySize; ++j)
    {
        float y = y1 + j * dy;
        for(auto i=0u; i < gaussian.m_xSize; ++i)
        {
            float x = x1 + i * dx;
            //gaussian[j][i] = exp(-(x*x + y*y)/(4*time));
            //std::cout << gaussian[j][i] << ", " << gaussian[j][i] * koef << std::endl;
            //gaussian[j][i] *= koef;
            //std::cout << gaussian[j][i] << std::endl;
            gaussian[j][i] = koef * (exp(-(x*x + y*y)*sigma));
            //gaussian[j][i] = 1/(2*M_PI) * exp(-(x*x+ y*y)/2);
        }
    }

    //saveMesh(gaussian, "gaussian", ".");

    int gkMidx = gaussian.m_xSize/2;
    int gkMidy = gaussian.m_ySize/2;
    for(auto j=0; j < gaussian.m_ySize; ++j)
    {
        for(auto i=0; i < gaussian.m_xSize; ++i)
        {
            for(auto kernelJ = 0; kernelJ < gaussian.m_ySize; ++kernelJ)
            {
                for(auto kernelI = 0; kernelI < gaussian.m_xSize; ++kernelI)
                {
                    int u0i =  i + gkMidx - kernelI;
                    int u0j =  j + gkMidy - kernelJ;
                    if( static_cast<unsigned int>(u0j) < u0.m_ySize && static_cast<unsigned int>(u0i) < u0.m_xSize )
                    {
                        //std::cout << "fallthrought:" << u0i << ", " << u0j << std::endl;
                        result[j][i] += gaussian[kernelJ][kernelI] * u0[u0j][u0i];
                    }
                    else 
                    {
                        //std::cout << "fallthrought false:" << kernelI<< ", " << kernelJ<< std::endl;
                        result[j][i] += gaussian[kernelJ][kernelI] * u0[0][0]; 
                    }
                }
            }
        }
    }
    auto gaussSize = static_cast<float>(gaussian.m_xSize * gaussian.m_ySize)/4.;
    for(auto y = 0u; y < result.m_ySize; ++y)
    {
        for(auto x = 0u; x < result.m_xSize; ++x)
        {
            result[y][x] /= gaussSize;
        }
    }
    auto end = std::chrono::high_resolution_clock::now();
    auto diff = end - start;
    std::cout << "Convolution took: " << std::chrono::duration<double, std::milli> (diff).count() << "ms" << std::endl;
    return result;
}

