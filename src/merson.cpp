#include "merson.h"
#include "helpers.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <chrono>

namespace paa
{
    Merson::Merson(MpiWrapper&mpi,
            BoundaryConditions& boundaryConditions,
            Initalizer &init,
            Vector2d& mesh,
            float simulationTime,
            float tau,
            float dx,
            float dy,
            std::string folderToSaveRes,
            int saveEveryIteration)
        : m_mpi(mpi), m_boundaryConditions(boundaryConditions),m_init(init), diffusion(dx,dy), m_mesh(mesh),
        m_simulationTime(simulationTime), tau(tau), dx(dx), dy(dy),
        folderToSaveRes(folderToSaveRes), saveEveryIteration(saveEveryIteration)
    {
        std::string mkdir ("mkdir ");
        system((mkdir + folderToSaveRes).c_str());
        m_xMin = (init.left!=-1) ? 0 : 1;
        m_xMax = (init.right!=-1) ? mesh.m_xSize : mesh.m_xSize-1;
        m_yMin = (init.bottom!=-1) ? 0 : 1;
        m_yMax = (init.top!=-1) ? mesh.m_ySize : mesh.m_ySize-1;
    }

void Merson::run() {
        Vector2d f(m_mesh.m_xSize, m_mesh.m_ySize, 0.);
        Vector2d k1(m_mesh.m_xSize, m_mesh.m_ySize, 0 );
        Vector2d k2(m_mesh.m_xSize, m_mesh.m_ySize, 0 );
        Vector2d k3(m_mesh.m_xSize, m_mesh.m_ySize, 0 );
        Vector2d k4(m_mesh.m_xSize, m_mesh.m_ySize, 0 );
        Vector2d k5(m_mesh.m_xSize, m_mesh.m_ySize, 0 );

        saveParallelMesh(m_mpi, m_mesh, m_init, "v", folderToSaveRes, iteration);

        auto start = std::chrono::high_resolution_clock::now();

        while( m_currentTime <  m_simulationTime )
        {
            runOneIter(f, k1, k2, k3, k4, k5);
        }

        auto end = std::chrono::high_resolution_clock::now();
        auto diff = end - start;
        std::cout << "It took: " << std::chrono::duration<double, std::milli> (diff).count() << "ms" << std::endl;

        std::cout << "Comms: "<<  m_boundaryConditions.commlength << "ms" << std::endl;
        std::cout << "Boundary recompution: "<<  m_boundaryConditions.boundaryRecomputation<< "ms" << std::endl;

        saveParallelMesh(m_mpi, m_mesh, m_init, "v",  folderToSaveRes, iteration);
        saveMesh(m_mesh, "vPart" + std::to_string(m_init.procRank)+"_", folderToSaveRes, iteration);
    }

    void Merson::runOneIter(
            Vector2d& f,
            Vector2d& k1,
            Vector2d& k2,
            Vector2d& k3,
            Vector2d& k4,
            Vector2d& k5
            )
    {


        auto & u = m_mesh;

        equation(k1,m_mesh);

        {
            auto const1 = 1./3.;
            for(auto y = m_yMin; y< m_yMax; ++y)
            {
                for(auto x = m_xMin; x< m_xMax; ++x)
                {
                    f[y][x] = const1 * k1[y][x] + u[y][x];
                }
            }
        }
         //std :: cout << "k1" << k1<< std::endl;
         //std :: cout << "f:" << f << std::endl;

        equation(k2, f);

        {
            auto const1 = 1./6.;
            for(auto y = m_yMin; y< m_yMax; ++y)
            {
                for(auto x = m_xMin; x< m_xMax; ++x)
                {
                    f[y][x] = u[y][x] + const1 * k1[y][x] + const1 * k2[y][x];
                }
            }
        }
         //std :: cout << "k2:" << k2<< std::endl;
         //std :: cout << "f:" << f << std::endl;

        equation(k3, f);

        {
            auto const1 = 1./8.;
            auto const2 = 3./8.;
            for(auto y = m_yMin; y< m_yMax; ++y)
            {
                for(auto x = m_xMin; x< m_xMax; ++x)
                {
                    f[y][x] = u[y][x] + const1 * k1[y][x] + const2 * k3[y][x];
                }
            }
        }
         //std :: cout << "k3:" << k3<< std::endl;
         //std :: cout << "f:" << f << std::endl;

        equation(k4,f);

        {
            auto const1 = 1./2.;
            auto const2 = 3./2.;
            for(auto y = m_yMin; y< m_yMax; ++y)
            {
                for(auto x = m_xMin; x< m_xMax; ++x)
                {
                    f[y][x] = u[y][x] + const1 * k1[y][x] 
                            - const2 * k3[y][x] + 2. * k4[y][x];
                }
            }
        }
         //std :: cout << "k4:" << k4<< std::endl;
         //std :: cout << "f:" << f << std::endl;

        equation(k5,f);
         //std :: cout << "k5:" << k5<< std::endl;

        {
            auto const1 = 1./5.;
            auto const2 = 9./10.;
            auto const3 = 4./5.;
            auto const4 = 1./10.;
            auto const5 = 1./3.;
            for(auto y = m_yMin; y< m_yMax; ++y)
            {
                for(auto x = m_xMin; x< m_xMax; ++x)
                {
                    f[y][x] = const5 * std::abs(const1 * k1[y][x] 
                            - const2 * k3[y][x] 
                            + const3 * k4[y][x] 
                            - const4 * k5[y][x]);
                }
            }
        }

        float E = std::numeric_limits<float>::min();
        for(auto y = m_yMin; y< m_yMax; ++y)
        {
            for(auto x = m_xMin; x< m_xMax; ++x)
            {
                if(f[y][x] > E)
                {
                    E = f[y][x];
                }
            }
        }

        if( m_init.mpiSize != 1 )
        {
            E = m_mpi.findMax(E);
        }

        float eps = 1e-4;
        //std::cout << m_init.procRank << "--E: " << E <<std::endl;
        if( E < eps )
        {
            auto const1 = 1./6.;
            for(auto y = m_yMin; y< m_yMax; ++y)
            {
                for(auto x = m_xMin; x< m_xMax; ++x)
                {
                    u[y][x] = u[y][x] + const1 * (k1[y][x] + 4. * k4[y][x] + k5[y][x] );
                }
            }

            m_currentTime += tau;
            iteration ++;
            if( (m_init.procRank == 0) && saveEveryIteration != 0 && (iteration % saveEveryIteration  == 0 ))
            {
                saveParallelMesh(m_mpi, m_mesh, m_init, "v", folderToSaveRes, iteration);
            }
        }

        auto omega = 0.85;
        tau = std::min<float>(tau * omega * pow(eps/E, 1./5.), m_simulationTime - m_currentTime );

        //std::cout << m_init.procRank <<"--tau: " << tau << std::endl;

        //if( m_init.procRank == 0 )
        //{
        //    std::cout << "Iteration " << iteration << " in time " 
        //              << m_currentTime << " done with tau: " << tau << std::endl;
        //}
    }

    void Merson::equation(Vector2d& u, Vector2d& f)
    {
        if( m_init.mpiSize != 1 )
        {
            m_boundaryConditions.startComms(f);
        }

        for(auto y = 1u; y < f.m_ySize - 1; ++y)
        {
            for(auto x = 1u; x < f.m_xSize - 1; ++x)
            {
                auto f_mid = f[y][x];
                auto f_left = f[y][x-1];
                auto f_right = f[y][x+1];
                auto f_bottom = f[y-1][x];
                auto f_top = f[y+1][x];
                u[y][x] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
            }
        }

        if( m_init.mpiSize != 1 )
        {
            m_boundaryConditions.computeMersonBoundary(u, f, tau);
        }
    }
}
