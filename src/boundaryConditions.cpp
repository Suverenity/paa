#include "boundaryConditions.h"
#include "helpers.h"

#include <cstring>
#include <chrono>

namespace paa
{
    BoundaryConditions::BoundaryConditions(MpiWrapper& mpi, Initalizer& init, Vector2d& mesh, int left,
            int right, int top, int bottom, float dx, float dy)
        : m_mpi(mpi), m_init(init), m_mesh(mesh), diffusion(dx,dy), m_dx(dx), m_dy(dy), m_left(left), m_right(right), 
        m_top(top), m_bottom(bottom)
    {
        if(left != -1 )
        {
            m_leftRecvBuf.resize(mesh.m_ySize, 0.);
            m_leftSendBuf.resize(mesh.m_ySize, 0.);
        }

        if(right != -1 )
        {
            m_rightRecvBuf.resize(mesh.m_ySize, 0.);
            m_rightSendBuf.resize(mesh.m_ySize, 0.);
        }

        if(top != -1 )
        {
            m_topRecvBuf.resize(mesh.m_xSize, 0.);
            m_topSendBuf.resize(mesh.m_xSize, 0.);
        }

        if(bottom != -1 )
        {
            m_bottomRecvBuf.resize(mesh.m_xSize, 0.);
            m_bottomSendBuf.resize(mesh.m_xSize, 0.);
        }
    }

    void BoundaryConditions::startComms(Vector2d &f){
        m_firedupRequests.resize(0.);
        auto start = std::chrono::high_resolution_clock::now();
        if( m_left != -1 )
        {
            for(auto y = 0u; y < f.m_ySize; ++y)
            {
                m_leftSendBuf[y] = f[y][0];
            }
            m_firedupRequests.push_back(m_mpi.sendParallel(m_leftSendBuf, m_left));
            m_firedupRequests.push_back(m_mpi.recieveParallel(m_leftRecvBuf, m_left));
        }

        if( m_right != -1 )
        {
            auto lastX = f.m_xSize-1;
            for(auto y = 0u; y < f.m_ySize; ++y)
            {
                m_rightSendBuf[y] = f[y][lastX];
            }
            m_firedupRequests.push_back(m_mpi.sendParallel(m_rightSendBuf, m_right));
            m_firedupRequests.push_back(m_mpi.recieveParallel(m_rightRecvBuf, m_right));
        }

        if( m_top != -1 )
        {
            std::memcpy(m_topSendBuf.data(), f[f.m_ySize-1], 
                    sizeof(float)*f.m_xSize);
            m_firedupRequests.push_back(m_mpi.sendParallel(m_topSendBuf, m_top));
            m_firedupRequests.push_back(m_mpi.recieveParallel(m_topRecvBuf, m_top));
        }
        if( m_bottom != -1 )
        {
            std::memcpy(m_bottomSendBuf.data(), f[0], 
                    sizeof(float)*f.m_xSize);
            m_firedupRequests.push_back(m_mpi.sendParallel(m_bottomSendBuf, m_bottom));
            m_firedupRequests.push_back(m_mpi.recieveParallel(m_bottomRecvBuf, m_bottom));
        }
        auto end = std::chrono::high_resolution_clock::now();
        auto diff = end - start;
        commlength += std::chrono::duration<double, std::milli> (diff).count();
    }

    void BoundaryConditions::computeMersonBoundary(Vector2d&u, Vector2d& f, float tau)
    {
        auto start = std::chrono::high_resolution_clock::now();
        m_mpi.waitall(m_firedupRequests);
        auto end = std::chrono::high_resolution_clock::now();
        auto diff = end - start;
        commlength += std::chrono::duration<double, std::milli> (diff).count();

        start = std::chrono::high_resolution_clock::now();
        // whole lines
        if( m_left != -1 )
        {
            for(auto y = 1u; y < m_mesh.m_ySize-1; ++y)
            {
                auto f_mid = f[y][0];
                auto f_left = m_leftRecvBuf[y];
                auto f_right = f[y][1];
                auto f_top = f[y+1][0];
                auto f_bottom = f[y-1][0];
                u[y][0] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
            }
        }

        if( m_right != -1 )
        {
            auto maxX = m_mesh.m_xSize-1;
            for(auto y = 1u; y < m_mesh.m_ySize-1; ++y)
            {
                auto f_mid = f[y][maxX];
                auto f_left = f[y][maxX-1];
                auto f_right = m_rightRecvBuf[y];
                auto f_top = f[y+1][maxX];
                auto f_bottom = f[y-1][maxX];
                u[y][maxX] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
            }
        }

        if( m_top != -1 )
        {
            auto maxY = m_mesh.m_ySize-1;
            for(auto x = 1u; x < m_mesh.m_xSize-1; ++x)
            {
                auto f_mid = f[maxY][x];
                auto f_left = f[maxY][x-1];
                auto f_right = f[maxY][x+1];
                auto f_top = m_topRecvBuf[x];
                auto f_bottom = f[maxY-1][x];
                u[maxY][x] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
            }
        }

        if( m_bottom != -1 )
        {
            for(auto x = 1u; x < m_mesh.m_xSize-1; ++x)
            {
                auto f_mid = f[0][x];
                auto f_left = f[0][x-1];
                auto f_right = f[0][x+1];
                auto f_top = f[1][x];
                auto f_bottom = m_bottomRecvBuf[x];
                u[0][x] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
            }
        }

        // corners
        auto xMaxId = m_mesh.m_xSize -1;
        auto yMaxId = m_mesh.m_ySize -1;

        // right bottom
        if( m_bottom != -1 && m_right != -1 )
        {
            auto f_top = f[1][xMaxId];
            auto f_left = f[0][xMaxId-1];
            auto f_mid = f[0][xMaxId];
            auto f_right = m_rightRecvBuf[0];
            auto f_bottom = m_bottomRecvBuf[xMaxId];
            u[0][xMaxId] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
        }

        //left bottom
        if( m_bottom != -1 && m_left != -1 )
        {
            auto f_top = f[1][0];
            auto f_left = m_leftRecvBuf[0];
            auto f_mid = f[0][0];
            auto f_right = f[0][1];
            auto f_bottom = m_bottomRecvBuf[0];
            u[0][0] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
        }

        // left top
        if( m_top != -1 && m_left != -1 )
        {
            auto f_top = m_topRecvBuf[0];
            auto f_left = m_leftRecvBuf[yMaxId];
            auto f_mid = f[yMaxId][0];
            auto f_right = f[yMaxId][1];
            auto f_bottom = f[yMaxId-1][0];
            u[yMaxId][0] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
        }

        // right top
        if( m_top != -1 && m_right != -1 )
        {
            auto f_top = m_topRecvBuf[xMaxId];
            auto f_left = f[yMaxId][xMaxId-1];
            auto f_mid = f[yMaxId][xMaxId];
            auto f_right = m_rightRecvBuf[yMaxId];
            auto f_bottom = f[yMaxId-1][xMaxId];
            u[yMaxId][xMaxId] = tau * diffusion(f_left, f_right, f_top, f_bottom, f_mid);
        }
        end = std::chrono::high_resolution_clock::now();
        diff = end - start;
        boundaryRecomputation += std::chrono::duration<double, std::milli> (diff).count();
    }
}
