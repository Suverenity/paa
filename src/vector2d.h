#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <vector>
#include <iostream>
#include <ostream>

#include "types.h"

namespace paa
{
    class Vector2d
    {
        public:
            using InternalVector = std::vector<float>;
            Vector2d(index_t xSize, index_t ySize, float initVal = 0.);

            //ideally there should be smth like gsl::span
            inline const float* operator[](index_t index) const { return &m_vec[m_xSize*index]; }
            inline float* operator[](index_t index){ return &m_vec[m_xSize*index]; }

            friend std::ostream& operator << (std::ostream& os, const Vector2d& v);

            void resize(int x, int y, float val = 0.);
            InternalVector& internalVector();

            index_t m_xSize;
            index_t m_ySize;
        private:
            InternalVector m_vec;
    };

} // namespace paa

#endif
