#ifndef MPIWRAPPER_H
#define MPIWRAPPER_H

#include <vector>

namespace paa
{
    class MpiWrapper
    {
        public:
            static MpiWrapper& getInstance(int argc, char** argv);

            int getSize();
            int getProcRank();
            void barrier();

            void gatherInts(int send, std::vector<int>& recieve);
            void gatherFloat(float send, std::vector<float>& recieve);
            void gatherArrays(std::vector<float>& partialArray, std::vector<float> &globalArray,
                    std::vector<int> expectedLenghts);

            void broadcastFloat(float *send);
            float findMax(float input);

            int sendParallel(std::vector<float>& input, int dest);
            int recieveParallel(std::vector<float>&output, int source);
            void waitall(std::vector<int> requests);
        private:
            MpiWrapper(int argc, char** argv);
            ~MpiWrapper();

            int m_procRank = -1;
            int m_mpiSize = -1;

    };
}

#endif
