#ifndef HELPERS_H
#define HELPERS_H

#include <vector>
#include <ostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "vector2d.h"
#include "initializer.h"

template<class VectorType>
std::ostream& operator <<(std::ostream &os, const std::vector<VectorType> &v)
{
    os << "[";
    for(auto it = v.begin(); it != v.end(); ++it)
    {
        os << " " << *it;
    }
    os << " ]";
    return os;
}
void saveMesh(paa::Vector2d& mesh, std::string prefix,  std::string path, 
        unsigned int iteration = 0);

void saveParallelMesh(paa::MpiWrapper& mpi,  paa::Vector2d& partMesh, 
        paa::Initalizer& init, std::string prefix,
        std::string path, unsigned int iteration=0);

paa::Vector2d computeConvolution(paa::Vector2d& u0, float time, float x1, 
        float dx, float y1, float dy);

#endif
