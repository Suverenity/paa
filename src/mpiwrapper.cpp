#include "mpiwrapper.h"
#include "helpers.h"

#include <mpi.h>
#include <stdexcept>
#include <iostream>

namespace paa 
{
    MpiWrapper& MpiWrapper::getInstance(int argc, char** argv)
    {
        static MpiWrapper mpi(argc, argv);
        return mpi;
    }

    void MpiWrapper::barrier()
    {
        if( MPI_Barrier(MPI_COMM_WORLD) != MPI_SUCCESS )
        {
            throw std::runtime_error("barrier failed");
        }
    }

    int MpiWrapper::getSize()
    {
        if( (m_mpiSize == -1) && MPI_Comm_size(MPI_COMM_WORLD, &m_mpiSize) != MPI_SUCCESS )
        {
            throw std::runtime_error("get Size failed");
        }
        return m_mpiSize;
    }

    int MpiWrapper::getProcRank()
    {
        if( (m_procRank == -1) && MPI_Comm_rank(MPI_COMM_WORLD, &m_procRank) != MPI_SUCCESS )
        {
            throw std::runtime_error("getProcRank failed");
        }
        return m_procRank;
    }

    float MpiWrapper::findMax(float input)
    {
        float output;
        if( MPI_Allreduce(&input, &output, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD) != MPI_SUCCESS )
        {
            throw std::runtime_error("getmax failed");
        }
        return output;
    }

    void MpiWrapper::gatherInts(int send, std::vector<int>& recieve)
    {
        if( MPI_Gather( &send, 1, MPI_INT, 
                    recieve.data(), 1, MPI_INT, 
                    0, MPI_COMM_WORLD) != MPI_SUCCESS)
        {
            throw std::runtime_error("gatherInts failed");
        }
    }

    void MpiWrapper::gatherFloat(float send, std::vector<float>& recieve)
    {
        if( MPI_Gather( &send, 1, MPI_FLOAT, 
                    recieve.data(), 1, MPI_FLOAT, 
                    0, MPI_COMM_WORLD) != MPI_SUCCESS)
        {
            throw std::runtime_error("gatherInts failed");
        }
    }

    void MpiWrapper::broadcastFloat(float* send)
    {
        if( MPI_Bcast(send, 1, MPI_FLOAT, 0, MPI_COMM_WORLD) != MPI_SUCCESS )
        {
            throw std::runtime_error("bcast float failed");
        }
    }

    void MpiWrapper::gatherArrays(std::vector<float>& partialArray, 
            std::vector<float> &globalArray, std::vector<int> expectedLenghts)
    {
        std::vector<int> displs;
        if( m_procRank == 0 )
        {
            displs.resize(m_mpiSize,0.);
            for (auto i = 1u; i < expectedLenghts.size(); ++i)
            {
                displs[i] = displs[i-1] + expectedLenghts[i-1];
            }
        }

        if( MPI_Gatherv(partialArray.data(), partialArray.size(), MPI_FLOAT,
                    globalArray.data(), expectedLenghts.data(), displs.data(),
                    MPI_FLOAT, 0, MPI_COMM_WORLD) != MPI_SUCCESS)
        {
            throw std::runtime_error("gatherv failed");
        }
    }

    int MpiWrapper::sendParallel(std::vector<float>& input, int dest)
    {
        MPI_Request r;
        if( MPI_Isend(input.data(), input.size(), MPI_FLOAT, dest, 
                    0, MPI_COMM_WORLD, &r) != MPI_SUCCESS )
        {
            throw std::runtime_error("isend failed");
        }
        return r;
    }

    int MpiWrapper::recieveParallel(std::vector<float>&output, int source)
    {
        MPI_Request r;
        if( MPI_Irecv(output.data(), output.size(), MPI_FLOAT, source, 0, 
                    MPI_COMM_WORLD, &r) != MPI_SUCCESS)
        {
            throw std::runtime_error("irecv failed");
        }
        return r;
    }
    void MpiWrapper::waitall(std::vector<int> requests)
    {
        if( MPI_Waitall(requests.size(), requests.data(), MPI_STATUSES_IGNORE) != MPI_SUCCESS )
        {
            throw std::runtime_error("waitall failed");
        }
    }

    MpiWrapper::MpiWrapper(int argc, char** argv)
    {
        MPI_Init(&argc, &argv);
        m_procRank = getProcRank();
        m_mpiSize = getSize();
    }

    MpiWrapper::~MpiWrapper()
    {
        MPI_Finalize();
    }
}
