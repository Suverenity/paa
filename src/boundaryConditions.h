#ifndef BOUNDARYCONDITIONS_H
#define BOUNDARYCONDITIONS_H

#include "vector2d.h"
#include "mpiwrapper.h"
#include "diffusion.h"
#include "initializer.h"

#include <fstream>

namespace paa
{
    class BoundaryConditions
    {
        public:
            BoundaryConditions(MpiWrapper& mpi, Initalizer& init, Vector2d& mesh, int left, int right,
                    int top, int bottom, float dx, float dy);

            // fires up all necessary for computation in current process
            void startComms(Vector2d& f);

            // waits until comms fired up earlier are finished
            void computeMersonBoundary(Vector2d& u, Vector2d& f, float tau);
            double commlength = 0.;
            double boundaryRecomputation = 0.;
        private:
            MpiWrapper& m_mpi;
            Initalizer &m_init;
            Vector2d& m_mesh;
            Diffusion diffusion;
            std::vector<int> m_firedupRequests;
            float m_dx, m_dy;
            int m_left, m_right, m_top, m_bottom;
            std::vector<float> m_leftRecvBuf, m_rightRecvBuf, m_topRecvBuf, m_bottomRecvBuf;
            std::vector<float> m_leftSendBuf, m_rightSendBuf, m_topSendBuf, m_bottomSendBuf;
    };
}

#endif
