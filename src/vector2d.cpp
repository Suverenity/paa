#include "vector2d.h"
#include "helpers.h"

#include <iostream>

namespace paa
{
    Vector2d::Vector2d(index_t xSize, index_t ySize, float initVal)
        : m_xSize(xSize), m_ySize(ySize), m_vec(xSize*ySize, initVal)
    {}

    std::ostream& operator << (std::ostream& os, const Vector2d& v)
    {
        for(auto y= 0u; y < v.m_ySize; ++y)
        {
            for(auto x= 0u; x < v.m_xSize; ++x)
            {
                os << v[y][x] << " ";
            }
            os << std::endl;

        }
        return os;
    }

    void Vector2d::resize(int x, int y, float val)
    {
        m_vec.resize(x*y, val);
        m_xSize = x;
        m_ySize = y;
    }

    Vector2d::InternalVector& Vector2d::internalVector()
    {
        return m_vec;
    }
} // namespace paa
