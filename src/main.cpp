#include <iostream>
#include <cstdlib>

#include "vector2d.h"
#include "helpers.h"
#include "merson.h"
#include "initializer.h"
#include "mpiwrapper.h"

//not mine: https://github.com/jarro2783/cxxopts
#include "cxxopts.hpp"

int main(int argc, char **argv)
{
    using namespace paa;

    MpiWrapper& mpi = MpiWrapper::getInstance(argc, argv);

    try{
        cxxopts::Options options("Paa", "semester task for parallel programming (PAA)");
        options
            .allow_unrecognised_options()
            .add_options()
            ("nx", "space discretization in x", 
             cxxopts::value< int>()->default_value("100"))
            ("ny", "space discretization in y",
             cxxopts::value< int>()->default_value("100"))
            ("x1", "left x interval", cxxopts::value<float>()->default_value("-1"))
            ("x2", "right x interval", cxxopts::value<float>()->default_value("1"))
            ("y1", "left y interval", cxxopts::value<float>()->default_value("-1"))
            ("y2", "right y interval", cxxopts::value<float>()->default_value("1"))
            ("time", "length of simulation",
             cxxopts::value<float>()->default_value("1"))
            ("si", "save results each iteration",
             cxxopts::value< int>()->default_value("0"))
            ("path", "where to store results", cxxopts::value<std::string>()->default_value("."))
            ("xg", "parallelization size in x", cxxopts::value<int>()->default_value("1"))
            ("yg", "parallelization size in y", cxxopts::value<int>()->default_value("1"))
            ("conv", "do convolution")
            ("h,help", "display help")
            ;

        auto parsedOpts = options.parse(argc, argv);
        if( parsedOpts.count("help") )
        {
            std::cout << options.help() << std::endl;
            return 0;
        }
        int Nx = parsedOpts["nx"].as<int>();
        int Ny = parsedOpts["ny"].as<int>(); 
        float x1 = parsedOpts["x1"].as<float>();
        float x2 = parsedOpts["x2"].as<float>();
        float y1 = parsedOpts["y1"].as<float>();
        float y2 = parsedOpts["y2"].as<float>();
        float time = parsedOpts["time"].as<float>();
        float si = parsedOpts["si"].as<int>();
        std::string path = parsedOpts["path"].as<std::string>();
        int xGrid = parsedOpts["xg"].as<int>();
        int yGrid = parsedOpts["yg"].as<int>();

        Initalizer init;
        int mpiSize =  mpi.getSize();
        int procRank = mpi.getProcRank();

        init.initParallelGridStats(mpi, procRank, mpiSize, x1, x2, y1, y2, Nx, Ny, xGrid, yGrid);

        Vector2d u(init.nxLocal, init.nyLocal, 0.);

        init.init(u);

        if( parsedOpts.count("conv") )
        {
            auto conv = computeConvolution(u, time, x1, init.dx, y1, init.dy);
            system((std::string("mkdir ") + path).c_str());
            saveMesh(conv, "conv", path, 0);
        }
        else
        {
            BoundaryConditions boundaryConditions(mpi, init,  u, init.left, init.right, init.top, 
                   init.bottom, init.dx, init.dy);

            Merson merson(mpi,boundaryConditions,init, u, time, 0.01, init.dx, init.dy, path, si);
            merson.run();
        }

    }
    catch(const cxxopts::OptionException& e)
    {
        std::cout << "error parsing: " << e.what() << std::endl;
    }

    return 0;
}
