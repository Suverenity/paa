#ifndef INITIALIZER_H
#define INITIALIZER_H

#include <math.h>

#include "vector2d.h"
#include "mpiwrapper.h"

namespace paa
{
    class Initalizer
    {
        public:
            void init(Vector2d& u)
            {
                for( auto j = 0u; j < u.m_ySize; ++j )
                {
                    auto y = y1Local + j * dy;
                    for( auto i = 0u; i < u.m_xSize; ++i )
                    {
                        auto x = x1Local + i * dx;
                        auto val = -sqrt(x*x +y*y) + 0.1;
                        u[j][i] = 1. + (0. < val) - (val < 0.);
                    }
                }
            }

            void initParallelGridStats(MpiWrapper &mpi, int procRank, int mpiSize, float x1, 
                    float x2, float y1, float y2, int nx, int ny, int xGrid, int yGrid)
            {
                this->mpiSize = mpiSize;
                this->procRank = procRank;
                if( mpiSize != xGrid * yGrid )
                {
                    throw std::runtime_error("cannot match processes to grid!");
                }

                this->xGrid = xGrid;
                this->yGrid = yGrid;
                x1Global = x1;
                x2Global = x2;
                y1Global = y1;
                y2Global = y2;
                nxGlobal = nx;
                nyGlobal = ny;

                dx = (x2-x1) / (nx);
                dy = (y2-y1) / (ny);

                procXCoord = procRank % xGrid;
                procYCoord = procRank / (mpiSize/yGrid);

                nxLocal = nx / xGrid;
                nyLocal = ny / yGrid;

                x1Local = x1 + procXCoord*nxLocal * dx;
                y1Local = y1 + procYCoord*nyLocal * dy;

                if( xGrid == procXCoord + 1 )
                {
                    nxLocal += nx%xGrid;
                }

                if( yGrid == procYCoord + 1 )
                {
                    nyLocal += ny%yGrid;
                }

                x2Local = x1Local + dx*nxLocal;
                y2Local = y1Local + dy*nyLocal;

                right = (procXCoord +1 == xGrid) ? -1 : procRank +1;
                left = (procXCoord == 0) ? -1 : procRank -1;
                top = (procYCoord +1 == yGrid) ? -1 : procRank + xGrid;
                bottom = (procYCoord == 0) ? -1 : procRank - xGrid;
            }

            float x1Global, x2Global;
            float y1Global, y2Global;

            float x1Local, x2Local;
            float y1Local, y2Local;

            float dx, dy;

            int left, right, top, bottom;

            int xGrid, yGrid;
            int nxGlobal, nyGlobal;
            int nxLocal, nyLocal;
            int procXCoord, procYCoord;
            int procRank;
            int mpiSize;
    };
}

#endif
