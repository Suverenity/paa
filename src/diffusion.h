#ifndef DIFFUSION_H
#define DIFFUSION_H


namespace paa 
{
    class Diffusion
    {
        public:
            Diffusion(float dx, float dy):m_dx(dx), m_dy(dy), m_dx2inv(1/(dx*dx)), m_dy2inv(1/(dy*dy)){}
            inline float operator()(float u_left, float u_right, float u_top, float u_bottom,
                    float u_mid)
            {
                return (u_left - 2.*u_mid + u_right) * m_dx2inv + (u_top -2.*u_mid + u_bottom) * m_dy2inv;
            }
            float m_dx, m_dy;
            float m_dx2inv, m_dy2inv;
    };
}

#endif
