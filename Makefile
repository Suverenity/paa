CXX=mpicxx
CXXFLAGS=-std=c++14 -Wall -Wpedantic 

SRC = $(wildcard src/*.cpp)
OBJ = $(SRC:.cpp=.o)
DEP = $(OBJ:.o=.d)

paaopt: CXXFLAGS += -O3
paagp: CXXFLAGS += -pg
paaoptgp: CXXFLAGS += -O3 -pg

.PHONY: paa
paa: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS)
	$(MAKE) cleandep

.PHONY: paaoptgp
paaoptgp: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS)
	$(MAKE) cleandep

.PHONY: paaopt
paaopt: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS)
	$(MAKE) cleandep

.PHONY: paagp
paagp: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS)
	$(MAKE) cleandep


-include $(DEP)

%.d: %.cpp
	@$(CXX) $(CXXFLAGS) $< -MM -MT $(@:.d=.o) >$@

.PHONY: cleandep
cleandep:
	rm -f $(DEP)

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f paaopt paa paaoptgp paagp
