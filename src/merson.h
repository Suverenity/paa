#ifndef MERSON_H
#define MERSON_H

#include <string>

#include "vector2d.h"
#include "types.h"
#include "diffusion.h"
#include "boundaryConditions.h"
#include "initializer.h"
#include "mpiwrapper.h"

namespace paa
{
    class Merson
    {
        public:
            Merson(MpiWrapper& mpi,
                   BoundaryConditions& BoundaryConditions,
                   Initalizer& init,
                   Vector2d& mesh,
                   float simulationTime,
                   float tau,
                   float dx,
                   float dy,
                   std::string folderToSaveRes, 
                   int saveEveryIteration);

            void run();
            void runOneIter(
                    Vector2d& f,
                    Vector2d& k1,
                    Vector2d& k2,
                    Vector2d& k3,
                    Vector2d& k4,
                    Vector2d& k5
                    );

        private:
            void equation(Vector2d& u, Vector2d& f);

            MpiWrapper &m_mpi;
            BoundaryConditions &m_boundaryConditions;
            Initalizer &m_init;

            Diffusion diffusion;
            float m_currentTime = 0.;
            index_t iteration = 0u;

            Vector2d& m_mesh;
            float m_simulationTime;
            float tau;

            float dx;
            float dy;

            index_t m_xMin, m_xMax;
            index_t m_yMin, m_yMax;

            std::string folderToSaveRes;
            int saveEveryIteration;
    };
}

#endif // MERSON_H
